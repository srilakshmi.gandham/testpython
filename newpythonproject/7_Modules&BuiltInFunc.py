# In python there are built in files called modules which contain sets of functions.

# Generic import
import random 
print(random.randint(1,10))

# Function import
from random import randint
print(randint(1,10))

# Universal import
from random import *
print(random())

from math import *
print(factorial(5))

# Built in Functions
print(abs(0))
print(abs(5))
print(abs(-3))
print(abs(-9))

print(type(1))
print(type(1.56))
print(type(True))
print(type("1"))

print(max(1,2,3,4,5))
print(max(1.5,1.0,1.8,1.9,1.2))
print(max("a","x","g"))
print(max("az","ax","ag"))
print(max("baz","bax","cag"))






