exp_list = ["This","is","new","python","project"]

def pass_list1(li):
    return li
print(pass_list1([1,2,3]))

def pass_list1(li):
    return li[2] +"test"

print(pass_list1(exp_list))

def pass_list2(li):
    return li[3]
print(pass_list2(exp_list))

numlist=[11,22,33,44,55,66]
def pass_list3(li):
    #return li[3]/0.5
    li.remove(22)
    li.insert(56,78)
    return li

print(pass_list3(numlist))

list_test = [1,2,4,3,6,5]
def print_items(a):
    for items in a:
        print(items)

print_items(list_test)

#range(stop)
var1 = range(5)
print(var1)

#range(start,stop)
var2 = range(1,4)
print(var2)

#range(start,stop,step)
var3 = range(2,20,3)
print(var3)

def passed(x):
    return(x)

print(passed(var1))

